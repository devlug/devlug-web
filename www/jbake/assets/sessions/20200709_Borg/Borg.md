![](img/borg.svg)

### 9. Juli 2020 @ /dev/LUG

Michael Weimann  
<small>
[mail@michael-weimann.eu](mail@michael-weimann.eu)  
[@mweimann:matrix.org](@mweimann:matrix.org)
</small>

----

## Inhalt

![](img/backup.jpeg)
<!-- .element: style="height: 25vh;" -->

* Historie
* Eigenschaften
* Konzepte
* Beispiel

---

# Historie

----

## Attic

* Backup Software
* Teil von Debian
* Entwicklung ab 2010
* Keine Commits seit 5 Jahren

----

## Attic Features

* Effizient
* Deduplizierung
* Komprimierung
* Verschlüsselung
* Metadaten

----

## Attic → Borg

* 2015 *Borg* Fork
* Gründe:
  * Offenere und
  * Schnellere Entwicklung
* Ergebnis
  * Attic: Keine Aktivität
  * Borg: Aktive Entwicklung
  
---

# Eigenschaften

----

## Allgemeines

* Open Source (BSD 3 Klausel)
* Maintainer "The Borg Collective"
* Einfach
* Für viele unix-artige BS verfügbar

----

## Effizient

* Deduplizierung
* Inkrementelle Sicherungen

----

## Schnell

* Teile in C/Cython
* Caching
* Schnelle Erkennung von Dateiänderungen

----

## Komprimierung

* Optional
* lz4 (Standard)
* zstd
* zlib
* lzma

→ Anwendungsfallspezifisch ausprobieren

----

## Speicherort

* Lokal oder per SSH
* Unabhängig vom Dateisystem
* Sicherungen können gemounted werden
* inkl. Metadaten

----

## Verschlüsselung

* Keine
* Schlüssel (Datei oder String)
* Signiert

----

# Konzepte

* Repository (URL) (wo)
* Archive (Backups mit Namen)

```
./datensenke
/irgendwo/im/nirgendwo/datensenke::backup1
ssh://benutzer@server:/var/backups
ssh://benutzer@server:/var/backups::backup1
```

---

# Beispiel

Repo anlegen

```
borg init --encryption repokey datensenke
borg list datensenke
```

----

## Ein paar Dateien sichern

```
echo "Hallo" >> dateien/datei1.txt
echo "Guude" >> dateien/datei2.txt

borg create datensenke::backup1 dateien

borg list datensenke
```

----

## `borg create` Optionen

```
--dry-run
--list
--comment "blubb"
```

----

## Mehr Backups

```
echo "Huhuuu" >> dateien/datei1.txt
borg create datensenke::backup2 dateien

echo "Servus" >> dateien/datei1.txt
borg create datensenke::backup3 dateien
```

----

## Geänderte Dateien anzeigen

```
borg list datensenke
borg diff datensenke::backup1 backup2
```

----

## Inhalte eines Archivs anzeigen

```
borg list datensenke::backup2
```

----

## Info anzeigen

```
borg info datensenke
```

----

## Archiv mounten

```
mkdir backup2
borg mount datensenke::backup2 backup2
cat backup2/dateien/datei1.txt
borg umount backup2
```

----

## Archiv entpacken

```
mkdir tmp
cd tmp
borg extract ../datensenke::backup2
```

Weitere Optionen (Auszug):

```
--exclude PATTERN
--list
--dry-run
```

----

## Archiv exportieren

```
borg export-tar datensenke::backup3 backup3.tar.gz
```

----

## Archiv prüfen

```
borg check datensenke
```

Weitere Optionen (Auszug):

```
--repository-only
--archives-only
--verify-data
--repair
```

----

## Archive aufräumen

```
borg prune --keep-last=1 datensenke
borg list datensenke
```

Weitere Optionen (Auszug):

```
--dry-run
--list
--keep-daily
--keep-weekly
```

----

## Benchmark

```
borg benchmark crud datensenke /tmp/bbench
```

* Braucht ca. 1GB Platz

----

## Umgebungsvariablen

z.B. zur Automatisierung

* `BORG_REPO`
* `BORG_PASSPHRASE`
* `BORG_RSH` ssh Kommando

---

## Wie funktionierts?

* Repository = Key/Value Store
* Ein paar Dateien (Auszug)
  * `config`
  * `data/`

----

![](img/object-graph.png)
<!-- .element: style="background-color: #fff;" -->

---

[Sehr gute Doku hier](https://borgbackup.readthedocs.io/en/stable/index.html)

![](img/fertig.jpeg)

