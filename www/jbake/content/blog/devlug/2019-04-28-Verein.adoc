= Vereinsgründung  
DebXWoody
2019-04-28
:jbake-type: post
:jbake-status: draft
:jbake-tags: devlug
:idprefix:
[abstract]
Informationen und Status der Vereinsgründung

== Motivation
Aktuell sind wir am prüfen, ob es sinnvoll ist einen gemeinnützigen
eingetragenen Verein zu gründen. Ohne eine Verein kommen wir immer wieder an
Punkte, welche sich aus verschiedenen Gründen nur schwer oder nicht umsetzen
lassen.

== Wie gehen wir vor?
Wir haben uns ein internes Projekt erstellt. Personen mit Interesse an einer
Vereinsgründung können Zugriff auf das Projekt haben und ihren Beitrag dazu
geben. Ab 01.06.2019 wollen wir uns den aktuell Stand ansehen und über das
weitere vorgehen sprechen.

== Status
Zur Zeit arbeiten wir parallel an mehrere Dinge. Der Fokus ist im Moment jedoch
zunächst die Erstellung eines Entwurfs für die Satzung, sowie ein Grobkonzept
der devLUG.

== Interesse?
Wer Interesse hat, kann einfach eine Mail an kontakt@devlug.de schreiben oder im
IRC #devlug im Freenode vorbei kommen.

