= GnuPlot 
DebXWoody
2018-06-30
:jbake-type: post
:jbake-status: published
:jbake-tags: gnuplot
:idprefix:
== Wie kann ich ein einfaches Digramm (Zeit / Anzahl) erstellen?
Erstelle eine Datei mit Datum und Anzahl. Hier ein Beispiel der Mitglieder auf der devlug Mailingliste ``members.dat``
----
2017-06-19 2
2018-03-15 3
2018-03-30 5
2018-06-30 14
----

Dann die ``members.plot`` Datei.

----
set title "Members" 
set terminal png
set output 'members.png'
set xdata time
set timefmt "%Y-%m-%d"
set xrange ['2017-06-01':'2018-06-30']
set yrange [2 : 30]
set style data line
set format x "%m.%y" 
set grid xtics ytics
plot 'members.dat' using 1:2 title "Members"
----

	gnuplot members.plot

image:https://www.devlug.de/img/devlug/members.png[] 

