= Router selber bauen 
plocki <plocki@devlug.de>
2021-06-11
:jbake-type: post
:jbake-status: published
:jbake-tags: router firewall 
:idprefix:
:toc:
[abstract]

Etwas Werbung für unser kleines Projekt: "Router selber bauen"

Hier geht es zu unserer Gitlab Projektseite: 
https://gitlab.com/devlug/devel/linux-router/

Und hier gibt es den Blog dazu: 
https://blog.jk-forensics.de/de/series/router-selber-bauen/

