= LineageOS
DebXWoody <debxwoody@devlug.de>
2019-06-09
:jbake-type: post
:jbake-tags: smartphone, android
:jbake-status: published 

== Was ist LineageOS?
LineageOS ist ein Open Source Betriebssystem für Smartphone auf Basis von
Android. Ein wesentlicher und wichtiger Vorteil an LineageOS ist, dass man das
Betriebssystem auch ohne Google Services verwenden kann. Es wird auch keine
Herstellerspezifische Software (Apps) installiert. Herstellerspezifische Treiber
und Firmware werden jedoch noch verwendet.
Dies ist ein wichtiger Aspekt bei
der Verwendung von Smartphones. Inspiriert von der
link:https://fsfe.org/campaigns/android/android.de.html[Befreie Dein Android!
Kampagne der FSFE]. Soll dieser Artikel noch ein paar weitere Informationen
geben.

== Hardware

* BQ Aquaris X (bardock)
* Sony Xperia XA2 (pioneer)

== F-Droid und Anwendungen
F-Droid ist ein App-Marktplatz als Alternative zu Play Store von Google. 
Ein paar Informationen findet man im Artikel 
link:https://www.zdf.de/nachrichten/heute/hintergrund-zu-f-droid-102.html[F-Droid richtig nutzten - und andere Fragen auf ZDF.de].

Welche Apps jemand braucht oder nicht braucht, muss jeder für sich selber entscheiden. Aus diesem Grund eine sehr grobe Übersicht.

* andOTP - App für One Time Passwörter (2FA) 
* Conversarions - Eine sehr gute Alternative zu WhatsAPP und anderen Instant Messaging Programmen. siehe auch link:https://www.devlug.de/blog/2019/2019-01-01-XMPP.html[XMPP / Jabber]
* Feeder - Ein RSS Reader
* K-9 Mail - Ein E-Mail Programm mit Unterstützung von OpenKeychain 
* MuPDF viewer - PDF Reader
* OpenKeychain - E-Mail und Dateien mit OpenPGP signieren und verschlüsseln (siehe auch link:https://www.devlug.de/blog/sicherheit/gpg.html[GnuPG]
* OsmAnd~ - OpenStreetMap Anwendung

Weitere Informationen findet ihr auf der Seite von Dirk link:https://www.deimeke.net/dirk/blog/index.php?/archives/3905-Apps-fuer-das-Smartphone-....html[Apps für das Smartphone ...].

== Literaturverzeichnis

* https://www.zdf.de/nachrichten/heute/hintergrund-zu-f-droid-102.html
* https://www.deimeke.net/dirk/blog/index.php?/archives/3905-Apps-fuer-das-Smartphone-....html

